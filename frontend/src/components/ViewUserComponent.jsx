import React, { Component } from 'react'
import UserService from '../services/UserService'

class ViewUserComponent extends Component {
    constructor(props) {
        super(props)

        this.state = {
            id: this.props.match.params.id,
            user: {}
        }
    }

    componentDidMount(){
        UserService.getUserById(this.state.id).then( res => {
            let data = res.data
            data.photo = "data:image/png;base64," + data.photo
            data.birthDate = this.formatDate(data.birthDate)
            this.setState({user: res.data});
        })
    }

    formatDate(date){
        date = date.split('T');
        date = `${date[0]}`;
        date = date.split('-');
        date = `${date[2]}/${date[1]}/${date[0]}`
        return date;
    }

    backToList(){
        this.props.history.push('/users');
    }

    render() {
        return (
            <div>
                <br></br>
                <div className = "row">
                    <button className="btn btn-primary" onClick={() => this.props.history.push('/users')}> Voltar</button>
                </div>
                <div className = "card col-md-6 offset-md-3">
                    <h3 className = "text-center"> Dados do Usuário</h3>
                    <div className = "card-body">
                        <div className = "row" >
                            <div class="rounded mx-auto d-block">
                                <img src={this.state.user.photo} width="100" height="100"/>
                            </div>
                        </div>
                        <div className = "row">
                            <label> Nome: </label>
                            <div> { this.state.user.name }</div>
                        </div>
                        <div className = "row">
                            <label> Data de Nascimento: </label>
                            <div> { this.state.user.birthDate }</div>
                        </div>
                    </div>

                </div>
            </div>
        )
    }
}

export default ViewUserComponent