import React, { Component } from 'react';
import UserService from '../services/UserService';

class FormUserComponent extends Component {
    constructor(props){
        super(props)

        this.state = {
            id: this.props.match.params.id ? this.props.match.params.id : null,
            name: '',
            birthDate: '',
            photo: ''
        }
    
        this.changeNameHandler = this.changeNameHandler.bind(this);
        this.changeBirthDateHandler = this.changeBirthDateHandler.bind(this);
        this.changePhotoHandler = this.changePhotoHandler.bind(this);
        this.saveOrUpdateUser = this.saveOrUpdateUser.bind(this);
    }

    componentDidMount(){
        if(this.state.id === '_add' || this.state.id === undefined || this.state.id === null || this.state.id === ""){
            return
        }else{
            UserService.getUserById(this.state.id).then( (res) =>{
                let user = res.data;
                user.birthDate = user.birthDate.split('T');
                user.birthDate = `${user.birthDate[0]}`;
                this.setState({name: user.name,
                    birthDate: user.birthDate,
                    photo : user.photo
                });
            });
        }        
    }

    saveOrUpdateUser = (e) => {
        e.preventDefault();
        
        let user = {name: this.state.name, birthDate: this.state.birthDate, photo: this.state.base64TextString};
        console.log('user => ' + JSON.stringify(user));
        if(this.state.id === '_add' || this.state.id === undefined || this.state.id === null || this.state.id === ""){
            UserService.createUser(user).then(res =>{
                this.props.history.push('/users');
            });
        }else{
            UserService.updateUser(user, this.state.id).then( res => {
                this.props.history.push('/users');
            });
        }
    }

    _handleReaderLoaded = (readerEvt) => {
        let binaryString = readerEvt.target.result
        this.setState({
            base64TextString: btoa(binaryString)
        })
    }

    changePhotoHandler= event => {
        let photo = event.target.files[0]

        if(photo){
            const reader = new FileReader();
            reader.onload = this._handleReaderLoaded.bind(this)
            reader.readAsBinaryString(photo)
        }
    }

    changeNameHandler= (event) => {
        this.setState({name: event.target.value})
    }

    changeBirthDateHandler= (event) => {
        this.setState({birthDate: event.target.value})
    }

    cancel(){
        this.props.history.push('/users');
    }

    getTitle(){
        if(this.state.id === '_add' || this.state.id === undefined || this.state.id === null || this.state.id === ""){
            return <h3 className="text-center">Adicionar Usuário</h3>
        }else{
            return <h3 className="text-center">Editar Usuário</h3>
        }
    }

    render() {    
        return (
            <div>
                <br></br>
                <div className = "row">
                    <button className="btn btn-primary" onClick={() => this.props.history.push('/users')}> Voltar</button>
                </div>
                <div className="container">
                    <div className="row">
                        <div className="card col-md-6 offset-md-3 offset-md-3">
                            <h3 className="text-center">{this.getTitle()}</h3>
                            <div className="card-body">
                                <form>
                                    <div className="form-group">
                                        <label>Nome:</label>
                                        <input placeholder="Nome Completo" name="name" className="form-control"
                                            value={this.state.name} onChange={this.changeNameHandler}/>
                                    </div>
                                    <div className = "form-group">
                                        <label>Data de Nascimento:</label>
                                        <input name="birthDate" className="form-control" type="date"
                                            value={this.state.birthDate} onChange={this.changeBirthDateHandler}></input>
                                    </div>
                                    <div className = "form-group">
                                        <label>Foto:</label>
                                        <input name="photo" id="file" className="form-control" type="file"
                                            accept=".jpeg, .png, .jpg" onChange={this.changePhotoHandler}></input>
                                    </div>
                                    <button className="btn btn-success" onClick={this.saveOrUpdateUser}>Salvar</button>
                                    <button className="btn btn-danger" onClick={this.cancel.bind(this)} style={{marginLeft: "10px"}}>Cancelar</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default FormUserComponent;