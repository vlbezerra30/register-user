import React, { Component } from 'react'
import UserService from '../services/UserService'

class ListUserComponent extends Component {
    constructor(props) {
        super(props)

        this.state = {
                users: []
        }
        this.addUser = this.addUser.bind(this);
        this.editUser = this.editUser.bind(this);
        this.deleteUser = this.deleteUser.bind(this);
    }

    deleteUser(id){
        UserService.deleteUser(id).then( res => {
            this.setState({users: this.state.users.filter(user => user.id !== id)});
        });
    }
    viewUser(id){
        this.props.history.push(`/view-user/${id}`);
    }
    editUser(id){
        this.props.history.push(`/form-user/${id}`);
    }

    componentDidMount(){
        UserService.getUsers().then((res) => {
            let data = res.data.map(item => {
                item.photo = "data:image/png;base64," + item.photo
                item.birthDate = this.formatDate(item.birthDate) 
                return item
            })
            this.setState({ users: data});
        });
    }

    formatDate(date){
        date = date.split('T');
        date = `${date[0]}`;
        date = date.split('-');
        date = `${date[2]}/${date[1]}/${date[0]}`
        return date;
    }

    addUser(){
        this.props.history.push('/form-user/_add');
    }

    render() {
        return (
            <div>
                 <h2 className="text-center">Lista de Usuários</h2>
                 <div className = "row">
                    <button className="btn btn-primary" onClick={this.addUser}> Adicionar Usuário</button>
                 </div>
                 <br></br>
                 <div className = "row">
                        <table className = "table table-striped table-bordered">

                            <thead>
                                <tr>
                                    <th> FOTO</th>
                                    <th> NOME</th>
                                    <th> DATA DE NASCIMENTO</th>
                                    <th> AÇÃO</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    this.state.users.map(
                                        user => 
                                        <tr key = {user.id}>
                                             <td> <img src={user.photo} width="50" height="50"/></td>   
                                             <td> {user.name} </td>   
                                             <td> {user.birthDate}</td>
                                             <td>
                                                 <button onClick={ () => this.editUser(user.id)} className="btn btn-info">Editar</button>
                                                 <button style={{marginLeft: "10px"}} onClick={ () => this.deleteUser(user.id)} className="btn btn-danger">Deletar</button>
                                                 <button style={{marginLeft: "10px"}} onClick={ () => this.viewUser(user.id)} className="btn btn-info">Ver Cadastro</button>
                                             </td>
                                        </tr>
                                    )
                                }
                            </tbody>
                        </table>

                 </div>

            </div>
        )
    }
}

export default ListUserComponent