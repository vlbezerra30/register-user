import './App.css';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import ListUserComponent from './components/ListUserComponent';
import HeaderComponent from './components/HeaderComponent';
import FooterComponent from './components/FooterComponent';
import FormUserComponent from './components/FormUserComponent';
import ViewUserComponent from './components/ViewUserComponent';

function App() {
  return (
    <div>
      <Router>
          <HeaderComponent />
          <div className="container">
            <Switch>
              <Route path = "/" exact component = {ListUserComponent}></Route>
              <Route path = "/users" component = {ListUserComponent}></Route>
              <Route path = "/form-user/:id" component = {FormUserComponent}></Route>
              <Route path = "/view-user/:id" component = {ViewUserComponent}></Route>
            </Switch>
          </div>
          <FooterComponent />
      </Router>
    </div>  
  );
}

export default App;
